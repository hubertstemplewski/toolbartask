
var MWPjs = {
	bar: function(barParams) {
		var message = barParams.message,
			close = barParams.close,
			clickOk = barParams.clickOk,
			position = barParams.position;

			var toolbar = document.createElement('div');
			

			var toolMessage = document.createElement('p');
			toolMessage.textContent = message;
			toolMessage.classList.add('message');
			toolMessage.id = 'message';
			toolbar.appendChild(toolMessage);

			var button = document.createElement('div');
			button.addEventListener('click', clickOk);
			toolbar.appendChild(button);

			var closeBtn = document.createElement('div');
			closeBtn.id = "close";
			closeBtn.addEventListener('click', close);
			toolbar.appendChild(closeBtn);
			

		 function barDown() {
          var heightBar = 0;
          var heightBtn = 0;
          function frame() {
            heightBar++;
            toolbar.style.height = heightBar + 'px';
            if(heightBar === 40) {
              clearInterval(animBar);
            }
          }
          function btn(){

            heightBtn++;
            button.style.height = heightBtn + 'px';

          	if(heightBtn === 15) {
              clearInterval(animBtn);
            }
          }
          var animBar = setInterval(frame, 15);
          var animBtn = setInterval(btn, 15);
        }

        button.textContent = "Click me!";

        toolbar.classList.add('toolbar');

        closeBtn.classList.add('close-icon');

        button.classList.add('btn');

		function bottomBar(){
			document.body.appendChild(toolbar);
          	toolbar.style.position = "fixed";
          	toolbar.style.bottom = "0";
          	barDown();
		}
		function topBar(){
			position = 'top';
			document.body.insertBefore(toolbar, document.body.childNodes[0]);
			toolbar.style.position = "relative";
          	toolbar.style.bottom = "0";
          	barDown();
		}

		var top = document.querySelector('#top');
			bottom = document.querySelector('#bottom');

		top.addEventListener('click', topBar);
		bottom.addEventListener('click', bottomBar);

		if (position === 'top') {
          document.body.insertBefore(toolbar, document.body.childNodes[0]);
          barDown();
        } else {
          document.body.appendChild(toolbar);
          toolbar.style.position = "fixed";
          toolbar.style.bottom = "0";
          barDown();
        }
	}

		
}